# Wirecard Configuration Management Challenge

This role is created to deploy a Tomcat9 web server behind a Nginx reverse proxy. The infrastructure choosen is a Centos7 instance on Google compute engine(GCE) or Vagrant.

[Molecule](https://molecule.readthedocs.io/en/stable/index.html) will be used to test the role against a Vagrant VM or a GCP instance.

Getting started
---------------
Clone the 'wirecard_challenge' repo from the ansible branch and change directory to wirecard_challenge

`root@ubuntu:~/home/abc# git clone https://bitbucket.org/kottapar/wirecard_challenge`

In order to run the tests on our role we'll install Molecule and Ansible in a Python virtualenv. Login to your VM and complete the below steps.

Install Python3, python3-dev and python3-venv using your package manager and create a virtual environment.

`python3 -m venv my_env`

Source the newly created environment

`source my_env/bin/activate`

Change the path to 'challenge-wirecard' and install the requirements. This installs Molecule which installs Ansible as a dependency.

`pip install -r requirements.txt`

---

Developing and testing this role
--------------------------------
*   This role can be
     * tested locally using a Vagrant VM on Virtualbox
     * tested and deployed in the cloud using GCP

PREREQUISITES
-------------
If you're on a Linux VM and would like to test the role locally, ensure that your hypervisor supports nested virtualization.

If you are using Virtualbox, note that nested virtualization is not supported for Intel chipsets yet.

If you're on Windows 10, then install the Windows subsystem for Linux (WSL) from the [Microsoft store](https://www.microsoft.com/store/productId/9N9TNGVNDL3Q).

Vagrant prerequisites
---------------------
1.  If your VM supports nested virtualization then:
    * Install [Virtualbox](https://www.virtualbox.org/wiki/Linux_Downloads) and [Vagrant](https://www.vagrantup.com/downloads.html) 
    
2.  However if your VM can't spin up Vagrant VMs on Virtualbox, then WSL is the way forward:
    * Install the Windows subsystem for Linux (WSL) from the [Microsoft store](https://www.microsoft.com/store/productId/9N9TNGVNDL3Q).
    * Install [Vagrant](https://www.vagrantup.com/downloads.html) in WSL. 
    * Install [Virtualbox](https://www.virtualbox.org/) and [Vagrant](https://www.vagrantup.com/downloads.html) on your Windows 10 machine.
        *  **NOTE**: Make sure you install the same version of Vagrant in WSL and the Windows machine
    * Execute `pip3 install python-vagrant` to install the python module for Vagrant if it's not already installed.
    * Execute `export VAGRANT_WSL_ENABLE_WINDOWS_ACCESS="1"` to enable WSL to use the Virtualbox from the Windows system.
    * Execute `export PATH="$PATH:/mnt/c/Program Files/Oracle/VirtualBox:/mnt/c/Windows/System32:/mnt/c/Windows/system32/WindowsPowerShell/v1.0"` to provide the paths to the Virtualbox, cmd and powershell executables.
    * For more info refer to the Vagrant WSL documentation [here](https://www.vagrantup.com/docs/other/wsl.html)

GCP prerequisites
-----------------
Register with GCP, create a project and then create a service account (ansible here) with the Compute Admin and Editor roles assigned. Generate and download the JSON key for this service account.

Edit the inventory/gce.ini and fill in the details from the JSON key.

Export the below environment variables. Replace them with your values. They're required by Molecule to spin up an instance for testing the role.

`export GCE_SERVICE_ACCOUNT_EMAIL="ansible@abcproject.iam.gserviceaccount.com" GCE_CREDENTIALS_FILE="~/keys/ansible_gcp.json" GCE_PROJECT_ID="abcproject-236712" ANSIBLE_REMOTE_TMP=/tmp/`

---

Testing the role
----------------
Scenarios
---------
Molecule uses [Scenarios](https://molecule.readthedocs.io/en/stable/getting-started.html#molecule-scenarios) to test the role against the Infra we define. We currently have two scenarios defined.

1.  default
    * The default scenario is configured to spin up an instance in GCE and test the role in the instance.
    * If you'd like to use this, then complete the `GCP pre-requisites`.
2.  vagrant
    * This scenario can be used if you'd like to test the role locally by spinning up a Vagrant VM in Virtualbox.
    * If you'd like to use this, then complete the `Vagrant pre-requisites`.

Vagrant
-------
Traverse to the directory `challenge-wirecard/roles/setup-deploy`. From this path you can run the molecule commands to test the role.

Run `molecule test -s vagrant --destroy never` to test the role. The `--destroy never` ensures that the Vagrant VM is not removed once the tests are completed.

GCP
---
Traverse to the directory `challenge-wirecard/roles/setup-deploy`. From this path you can run the molecule commands to test the role.

Run `molecule test` to test the role. Molecule will create a new GCP instance and will perform the tests listed in `roles/setup-deploy/molecule/default/tests/`.

---

Deploying the role
------------------
VM
--
If you'd like to deploy the role to a local VM in your network, then from your Ansible server run 

`ansible-playbook -l prwebapp01 webapp-deploy.yml`

or `ansible-playbook -l prwebapp01 -e "undeploy=yes" webapp-deploy.yml` to undeploy and deploy an updated package.
  
GCP
---
We're using the Ansible dynamic inventory to specify the host against which we'll run this role. 

Go to GCP and provision a **n1-standard-1 with a Centos7 image**. Make sure you scroll down and enable the **'Allow HTTP' and 'Allow HTTPS'** rules. 

Now run `./inventory/gce.py --list --pretty` You should see a listing of your GCP instance details. You can also try doing a ping test using Ansible.

`(my_env) root@ubuntu:/challenge-wirecard#  ansible -i inventory/ vm01 -m ping`

Once the tests are done we can proceed to deploy our role on the GCP instance. Traverse to the directory challenge-wirecard and run 

`ansible-playbook -i inventory/ -l vm01 webapp-deploy.yml`

Once the deployment is successful, head over to your GCP console -> Compute engine -> VM instances and copy the external ip of the VM. Then in a browser tab navigate to `https://<ip>/hello`

If you'd like to deploy an updated version of the app, execute `ansible-playbook -i inventory/ -l vm01 -e "undeploy=yes" webapp-deploy.yml`
